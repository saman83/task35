import React from 'react';
import LoginForm from '../forms/LoginForm'

const Login = () => {
    return (
        <div>
            <h1>Login to the Survey Puppy</h1>

            <LoginForm />
        </div>
    )
};

export default Login