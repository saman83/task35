import React, { useState } from "react";
import { registerUser } from '../../api/user.api'


const RegisterForm = (props) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [ isLoading, setIsLoading ] = useState(false);
  const [ registerError , setRegisterError ] = useState('');

  const onRegisterClicked = async (ev) => {
    
    setIsLoading(true);

    try {
        const result = await registerUser(username, password);
        console.log(result);
        
    } catch (e) {
        setRegisterError(e.message || e);
    } finally {
        setIsLoading(false)
    }
    
      

    
  };

  const onUsernameChanged = (ev) => setUsername(ev.target.value.trim());
  const onPasswordChanged = (ev) => setUsername(ev.target.value.trim());

  return (
    <form>
      <div>
        <label>Username: </label>
        <input
          type="text"
          placeholder="Enter a username.."
          onChange={onUsernameChanged}
        />
      </div>

      <div>
        <label>Password: </label>
        <input
          type="password"
          placeholder="Enter a password.."
          onChange={onPasswordChanged}
        />
      </div>

      <div>
        <button type="button" onClick={onRegisterClicked}>
          Register
        </button>
      </div>

        { isLoading && <div>Registering user..</div> }

  { registerError && <div>{registerError}</div> }


    </form>
  );
};

export default RegisterForm;
